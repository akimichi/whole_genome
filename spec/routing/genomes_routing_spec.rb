require "spec_helper"

describe GenomesController do
  describe "routing" do

    it "routes to #index" do
      get("/genomes").should route_to("genomes#index")
    end

    it "routes to #new" do
      get("/genomes/new").should route_to("genomes#new")
    end

    it "routes to #show" do
      get("/genomes/1").should route_to("genomes#show", :id => "1")
    end

    it "routes to #edit" do
      get("/genomes/1/edit").should route_to("genomes#edit", :id => "1")
    end

    it "routes to #create" do
      post("/genomes").should route_to("genomes#create")
    end

    it "routes to #update" do
      put("/genomes/1").should route_to("genomes#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/genomes/1").should route_to("genomes#destroy", :id => "1")
    end

  end
end
