require 'spec_helper'

describe "genomes/index" do
  before(:each) do
    assign(:genomes, [
      stub_model(Genome,
        :format => "Format"
      ),
      stub_model(Genome,
        :format => "Format"
      )
    ])
  end

  it "renders a list of genomes" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Format".to_s, :count => 2
  end
end
