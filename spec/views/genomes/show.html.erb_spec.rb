require 'spec_helper'

describe "genomes/show" do
  before(:each) do
    @genome = assign(:genome, stub_model(Genome,
      :format => "Format"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Format/)
  end
end
