require 'spec_helper'

describe "genomes/edit" do
  before(:each) do
    @genome = assign(:genome, stub_model(Genome,
      :format => "MyString"
    ))
  end

  it "renders the edit genome form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", genome_path(@genome), "post" do
      assert_select "input#genome_format[name=?]", "genome[format]"
    end
  end
end
