require 'spec_helper'

describe "genomes/new" do
  before(:each) do
    assign(:genome, stub_model(Genome,
      :format => "MyString"
    ).as_new_record)
  end

  it "renders new genome form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", genomes_path, "post" do
      assert_select "input#genome_format[name=?]", "genome[format]"
    end
  end
end
