require 'spec_helper'

describe "users/index" do
  before(:each) do
    assign(:users, [
      stub_model(User,
        :healthcare_identifier => "Healthcare Identifier"
      ),
      stub_model(User,
        :healthcare_identifier => "Healthcare Identifier"
      )
    ])
  end

  it "renders a list of users" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Healthcare Identifier".to_s, :count => 2
  end
end
