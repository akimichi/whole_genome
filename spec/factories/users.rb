# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    ehr_identifier "MyString"
    healthcare_identifier "MyString"
  end
end
