# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

user = User.create(ehr_identifier: "123", healthcare_identifier: "abc")
File.open("db/resources/10Gen_Venter_SNV.gvf") do |file|
  Genome.create(format: "GVF",
                content: file,
                user: user)
end
# genome.save!
# genome = Genome.new(format: "GVF",
#                     content: file.read,
#                     user: user)
# genome.save!
