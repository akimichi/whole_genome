collection @genomes
attributes :format

code(:content) do |genome|
  {:location => "#{content_api_v1_user_genome_url(genome.user.ehr_identifier, genome)}.json"}
end
