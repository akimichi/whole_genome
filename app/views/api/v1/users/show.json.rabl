object @user
attributes :ehr_identifier, :healthcare_identifier

code(:genomes) do |user|
  {:link => {:rel => "self", :href => "#{api_v1_user_genomes_url(user.ehr_identifier)}.json"}}
end
