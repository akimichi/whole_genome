collection @users
attributes :ehr_identifier, :healthcare_identifier

code(:link) do |user|
  {:rel => "self", :href => "#{api_v1_user_url(user.ehr_identifier)}.json"}
end
