class Api::V1::GenomesController < ApplicationController
  respond_to :json

  # GET /genomes
  # GET /genomes.json
  def index
    @genomes = user.genomes

    # respond_to do |format|
    #   format.html # index.html.erb
    #   format.json { render json: @genomes }
    # end
  end

  def content
    content = Genome.find(params[:id]).content
    response.etag = [content.file.md5]
    if request.fresh?(response)
      head :not_modified
    else
      send_data content.read
    end
  end
  def upload 
    uploaded_io = params[:genome][:content]
    File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'w') do |file|
      file.write(uploaded_io.read)
    end
  end

  private

  def user
    @user = User.where(:ehr_identifier => params[:id])
  end
end
