class User
  include Mongoid::Document
  field :ehr_identifier
  field :healthcare_identifier

  has_many :genomes
  # embeds_many :genomes, cascade_callbacks: true
end
