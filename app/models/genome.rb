class Genome
  include Mongoid::Document
  include Mongoid::Timestamps::Updated

  field :format, type: String
  # embedded_in :user # , :inverse_of => :provider
  belongs_to :user # , :inverse_of => :provider

  mount_uploader :content, GenomeUploader


end
